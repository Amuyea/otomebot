# Love 365 Commands
The bot's default prefix is `o?` and can use slash command `/`

## Help
* help - Help Menu `help`
    * Example:
 
        o?help jst
    
    * Help Menu's Categorizes `help (Categorizes)`

        o?help BirthdayInfo

Now we can move on to each categorize and their commands!

## BirthdayInfo
* bdemtest - Make & view Embed `{Character Name} {Character's Game Title} {Quote} {Image Link}`
    * Example:

        o?bdemtest Soryu Oh, Kissed by the Baddest Bidder, "It's my birthday, where is my gift?", ~~image link with .png/.jpg/.gif etc~~

* bdparty - **Server Owner/Mods with Manage Message Permission** Set channel to autopost birthdays in Japan time 
    * Example:

        o?bdparty #Channel

* birthdaytime - Check all Birthday of date
    * Example:

        o?findcharbd monthname 2022-06-18

* checkbdcg - Show Characters' BD by Month to see if they have an unique CG
    * Example:

        o?findcharbd June

* checkbdq - Show Characters' BD by Month to see if they have an unique Birthday Quote
    * Example:

        o?findcharbd July

* findcharbd - Show All Characters' BD by Month
    * Example:

        o?findcharbd March

* jst - Just to check JST Time
    * Example:

        o?jst

* missbirthdaytime - **BOT OWNER ONLY** Post Character Birthday(s) in specific channel if the auto doesn't work
    * Example:

        o?missbirthdaytime

## Misc.
* avatar - 
    * Example:

        o?avatar

* beautiful - 
    * Example:

        o?beautiful

* parrot - 
    * Example:

        o?parrot

* serverinfo - 
    * Example:

        o?serverinfo

* spotify - 
    * Example:

        o?spotify

* wanted - 
    * Example:

        o?wanted

* whothis - 
    * Example:

        o?whothis

* clearmessagesdm - *DM ONLY* Use this command to remove Bot's messages from your DM
    * Example:

        o?clearmessagesdm

## Prefix
* prefix - Check your server's prefix (Can @bot prefix)
    * Example:

        o?prefix

* changeprefix **Server Owner/Mods with Manage Message Permission** Change your server's prefix
    * Example:

        o?changeprefix o!


## ReviewInfo
* addbad **BOT OWNER ONLY** Add "offensive" word to DB

* bad - Show list of "offensive" words
    * Example:

        o?bad

* removebad **BOT OWNER ONLY** Remove "offensive" word from DB

* review - Checks your review for offensive
    * Example:

        o?review I love Voltage!


## TitleInfo
* findtitle - Show Title Info (forgot full name)
    * Example:

        o?findtitle kiss

* searchtag - Find a matching tag
    * Example:

        o?titlesum mature

* titlesum - Show Title Info (If you know full title name or short title)
    * Example:

        o?titlesum MK


## Walkthrough *In Progress*
* addwalkdb
    * Example:

        o?

* findwalkdb
    * Example:

        o?

* removewalkdb
    * Example:

        o?

* walk
    * Example:

        o?

* walks
    * Example:

        o?


## CharacterInfo
* charsum - Simple Show Character Info
    * Example:

        o?charsum Soryu

* findchar - Show Character Info (forgot full name)
    * Example:

        o?findchar Kaga

* findtchar - Show Title's Character
    * Example:

        o?findtchar RMD


## No Category
* sync **BOT OWNER ONLY**
